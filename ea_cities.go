package ea

func newCity(name string) cityRef {
	e := cityRef{name, []*cityMap{}}
	return e
}

func getCity(cityName string) *cityRef {
	return cities.m[cityName]
}

func getCityMap(cityName string) *cityMap {
	return citiesMap.m[cityName]
}

func addCityReference(cityName string, referenceCityMap *cityMap) {
	if cityName != "" {
		cityFound, found := cities.m[cityName]
		if !found {
			newCity := newCity(cityName)
			cities.m[cityName] = &newCity
			newCity.indexReferences = append(newCity.indexReferences, referenceCityMap)
		} else {
			cityFound.indexReferences = append(cityFound.indexReferences, referenceCityMap)
		}
	}
}

func newCityMap(source, nord, south, ovest, west string) cityMap {
	e := cityMap{source, nord, south, ovest, west}
	return e
}

func AddNewCityMapT(source, nord, south, ovest, west string) *cityMap {
	citiesMap.Lock()
	r := AddNewCityMap(source, nord, south, ovest, west)
	citiesMap.Unlock()
	return r

}
func AddNewCityMap(source, nord, south, ovest, west string) *cityMap {
	cityMap := newCityMap(source, nord, south, ovest, west)
	addCityReference(source, &cityMap)
	addCityReference(nord, &cityMap)
	addCityReference(south, &cityMap)
	addCityReference(ovest, &cityMap)
	addCityReference(west, &cityMap)
	citiesMap.m[source] = &cityMap
	return &cityMap
}

func cleanReference(cityRefCheck string) {
	if cityRefCheck != "" {
		ct := getCity(cityRefCheck)

		for _, v := range ct.indexReferences {
			if ct.name == v.nordCity {
				v.nordCity = ""
			}

			if ct.name == v.southCity {
				v.southCity = ""
			}

			if ct.name == v.ovestCity {
				v.ovestCity = ""
			}

			if ct.name == v.westCity {
				v.westCity = ""
			}
		}
	}
}

func deleteCityMapT(cityMapRef cityMap) {
	citiesMap.Lock()
	deleteCityMap(cityMapRef)
	citiesMap.Unlock()
}

func deleteCityMap(cityMapRef cityMap) {
	cleanReference(cityMapRef.sourceCity)
	cleanReference(cityMapRef.nordCity)
	cleanReference(cityMapRef.southCity)
	cleanReference(cityMapRef.ovestCity)
	cleanReference(cityMapRef.westCity)
	delete(citiesMap.m, cityMapRef.sourceCity)
}

func CitiesMapLenT() int {
	citiesMap.Lock()
	ln := len(citiesMap.m)
	citiesMap.Unlock()
	return ln
}

func CitiesLenT() int {
	cities.RLock()
	ln := len(cities.m)
	cities.RUnlock()
	return ln
}

func cleanCities() {
	for k := range cities.m {
		delete(cities.m, k)
	}
}

func cleanCitiesMap() {
	for k := range citiesMap.m {
		delete(citiesMap.m, k)
	}
}

func cleanCitiesMapT() {
	citiesMap.Lock()
	cleanCitiesMap()
	citiesMap.Unlock()
}

func CleanCitiesT() {
	cities.Lock()
	cleanCities()
	cities.Unlock()
}
