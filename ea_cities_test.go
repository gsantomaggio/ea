package ea

import (
	"strconv"
	"sync"
	"testing"
	_ "testing"
)

func checkCityAndRefCount(ref *cityRef, nameWant string, lenWant int) bool {
	return ref.name == nameWant && lenWant == len(ref.indexReferences)
}

func TestAddNewCityMap(t *testing.T) {

	cityMap := AddNewCityMap("Foo", "Bar", "Qu-ux", "", "Baz")
	cityBar := getCity("Bar")

	if !checkCityAndRefCount(cityBar, "Bar", 1) {
		t.Errorf("Incorrect city, got: %s, want: %s, got ref %d, want: %d", cityBar.name,
			"Bar", len(cityBar.indexReferences), 1)
	}

	cityBaz := getCity("Baz")

	if !checkCityAndRefCount(cityBaz, "Baz", 1) {
		t.Errorf("Incorrect city, got: %s, want: %s, got ref %d, want: %d", cityBaz.name,
			"Bar", len(cityBaz.indexReferences), 1)
	}

	if cityBaz.indexReferences[0] != cityMap {
		t.Errorf("Incorrect pointers cityMap")
	}

	if cityBar.indexReferences[0] != cityMap {
		t.Errorf("Incorrect pointers cityMap")
	}
	cleanCitiesMapT()
	CleanCitiesT()
}

//
////Foo north=Bar West=Baz South=Qu-ux
////Bar South=Foo West=Bee
//
////Foo west=Baz south=Qu-ux
//
func TestSimpleDeleteCityMap(t *testing.T) {
	cityMapFoo := AddNewCityMap("Foo", "Bar", "Qu-ux", "", "Baz")
	cityMapBar := AddNewCityMap("Bar", "", "Foo", "", "Bee")
	deleteCityMap(*cityMapBar)
	if !(cityMapFoo.sourceCity == "Foo" && cityMapFoo.westCity == "Baz" && cityMapFoo.southCity == "Qu-ux" &&
		cityMapFoo.ovestCity == "" && cityMapFoo.nordCity == "") {
		t.Errorf("Incorrect TestDeleteCityMap cityMap")
	}

	if CitiesMapLenT() != 1 {
		t.Errorf("Incorrect city, got: %d, want: %d ",
			CitiesMapLenT(), 1)
	}
	cleanCitiesMapT()
	CleanCitiesT()
}

func TestMultipleDeleteCityMap(t *testing.T) {
	const records = 1500

	var citiesFooAdded [] *cityMap
	for i := 1; i <= records; i++ {
		cityMapFoo := AddNewCityMap("Foo_"+strconv.Itoa(i), "Bar_"+strconv.Itoa(i), "Qu-ux", "", "Baz")
		citiesFooAdded = append(citiesFooAdded, cityMapFoo)
	}

	var citiesBarAdded [] *cityMap
	for i := 1; i <= records; i++ {
		cityMapBar := AddNewCityMap("Bar_"+strconv.Itoa(i), "", "Foo", "", "Bee")
		citiesBarAdded = append(citiesBarAdded, cityMapBar)
	}

	for _, v := range citiesBarAdded {
		deleteCityMap(*v)
	}

	for _, cityMapFoo := range citiesFooAdded {
		if !(cityMapFoo.westCity == "Baz" && cityMapFoo.southCity == "Qu-ux" &&
			cityMapFoo.ovestCity == "" && cityMapFoo.nordCity == "") {
			t.Errorf("Incorrect Test Multiple Delete CityMap cityMap")
		}
	}

	if CitiesMapLenT() != records {
		t.Errorf("Incorrect city, got: %d, want: %d ",
			CitiesMapLenT(), records)
	}

	for _, v := range citiesFooAdded {
		deleteCityMap(*v)
	}

	if CitiesMapLenT() != 0 {
		t.Errorf("Incorrect city, got: %d, want: %d ",
			CitiesMapLenT(), 0)
	}

}

func TestCheckConsistency(t *testing.T) {

	const records = 3
	var wg sync.WaitGroup
	wg.Add(records)
	for i := 0; i < records; i++ {
		go func(v int) {
			defer wg.Done()
			AddNewCityMapT("Foo_"+strconv.Itoa(v), "Bar_"+strconv.Itoa(v), "Qu-ux", "", "Baz")
		}(i)
	}
	wg.Wait()

	wg.Add(records)

	for i := 0; i < records; i++ {
		go func(v int) {
			defer wg.Done()
			AddNewCityMapT("Bar_"+strconv.Itoa(v), "", "Foo", "", "Bee")
		}(i)
	}
	wg.Wait()

	wg.Add(records)
	for i := 0; i < records; i++ {
		go func(v int) {
			defer wg.Done()
			p := getCityMap("Bar_" + strconv.Itoa(v))
			deleteCityMapT(*p)
		}(i)
	}
	wg.Wait()

	for i := 0; i < records; i++ {
		{
			cityMapFoo := getCityMap("Foo_" + strconv.Itoa(i))
			if !(cityMapFoo.westCity == "Baz" && cityMapFoo.southCity == "Qu-ux" &&
				cityMapFoo.ovestCity == "" && cityMapFoo.nordCity == "") {
				t.Errorf("Incorrect Test Multiple Delete CityMap cityMap")
			}
		}
	}

	if CitiesMapLenT() != records {
		t.Errorf("Incorrect city, got: %d, want: %d ",
			CitiesMapLenT(), records)
	}


	wg.Add(records)
	for i := 0; i < records; i++ {
		go func(v int) {
			defer wg.Done()
			p := getCityMap("Foo_" + strconv.Itoa(v))
			deleteCityMapT(*p)
		}(i)
	}
	wg.Wait()

	if CitiesMapLenT() != 0 {
		t.Errorf("Incorrect city, got: %d, want: %d ",
			CitiesMapLenT(), 0)
	}


}
