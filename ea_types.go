package ea

import (
	"sync"
)

//Foo north=Bar west=Baz south=Qu-ux
//Bar south=Foo west=Bee

//Foo west=Baz south=Qu-ux

type cityRef struct {
	name            string
	indexReferences [] *cityMap
}

var cities = struct {
	sync.RWMutex
	m map[string]*cityRef
}{m: make(map[string]*cityRef)}

type cityMap struct {
	sourceCity string
	nordCity   string
	southCity  string
	ovestCity  string
	westCity   string
}

var citiesMap = struct {
	sync.RWMutex
	m map[string]*cityMap
}{m: make(map[string]*cityMap)}

type alien struct {
	Id int
}
