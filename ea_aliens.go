package ea

import (
	"sync"
)

var aliens = struct {
	sync.RWMutex
	m map[int]alien
}{m: make(map[int]alien)}

var aliensPositions = struct {
	sync.RWMutex
	m map[string][] alien
}{m: make(map[string][] alien)}

func NewAlien(id int) alien {
	e := alien{id}
	return e
}

func AddAlien(alien alien) {
	aliens.m[alien.Id] = alien
}

func AddAlienT(alien alien) {
	aliens.Lock()
	aliens.m[alien.Id] = alien
	aliens.Unlock()
}

func DeleteAlien(id int) {
	delete(aliens.m, id)
}

func DeleteAlienT(id int) {
	aliens.Lock()
	delete(aliens.m, id)
	aliens.Unlock()
}

func GetAlienById(id int) alien {
	return aliens.m[id]
}

func MoveAlien(city string, alien2 alien) {
	

}

func AddAlienPosition(){


}